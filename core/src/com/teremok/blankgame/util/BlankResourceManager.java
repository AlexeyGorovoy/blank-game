package com.teremok.blankgame.util;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.teremok.blankgame.BlankGame;
import com.teremok.framework.util.ResourceManagerImpl;

/**
 * Created by Алексей on 09.07.2014
 */
public class BlankResourceManager extends ResourceManagerImpl <BlankGame> {

    public BlankResourceManager(BlankGame game) {
        super(game);
    }

    @Override
    public void preload() {
        assetManager.load("atlas/background.pack", TextureAtlas.class);
        assetManager.load("atlas/screen.pack", TextureAtlas.class);
    }
}
