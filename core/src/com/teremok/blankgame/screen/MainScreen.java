package com.teremok.blankgame.screen;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.teremok.blankgame.BlankGame;
import com.teremok.framework.screen.StaticScreen;
import com.teremok.framework.ui.CheckboxTexture;
import com.teremok.framework.util.Localizator;

/**
 * Created by Алексей on 09.07.2014
 */
public class MainScreen extends StaticScreen <BlankGame> {

    private final static String SOME_PICTURE = "somepicture";

    CheckboxTexture checkboxTexture;

    public MainScreen(BlankGame game, String filename) {
        super(game, filename);
    }

    @Override
    protected void addActors() {
        checkboxTexture = new CheckboxTexture(uiElements.get(SOME_PICTURE));
        stage.addActor(checkboxTexture);
    }

    @Override
    protected void addListeners() {
        stage.addListener( new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                return stage.hit(x,y,true) instanceof CheckboxTexture;
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                if (checkboxTexture.isChecked()) {
                    checkboxTexture.unCheck();
                } else {
                    checkboxTexture.check();
                }
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                if (keycode == Input.Keys.BACK || keycode == Input.Keys.ESCAPE) {
                    game.getScreenController().gracefullyExitGame();
                }

                if (keycode == Input.Keys.L) {
                    Localizator.switchLanguage();
                }

                return super.keyUp(event, keycode);
            }
        });
    }
}
