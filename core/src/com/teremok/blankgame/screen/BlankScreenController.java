package com.teremok.blankgame.screen;

import com.teremok.blankgame.BlankGame;
import com.teremok.framework.screen.ScreenControllerImpl;
import com.teremok.framework.screen.StaticScreen;

import static com.teremok.blankgame.screen.Screens.*;

/**
 * Created by Алексей on 09.07.2014
 */
public class BlankScreenController extends ScreenControllerImpl <BlankGame> {

    public BlankScreenController(BlankGame game) {
        super(game);
    }

    @Override
    public StaticScreen resolve(String s) {
        StaticScreen screen = null;
        switch (s) {
            case MAIN:
                screen = new MainScreen(game, MAIN);
        }
        return screen;
    }
}
