package com.teremok.blankgame;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.teremok.blankgame.screen.BlankScreenController;
import com.teremok.blankgame.screen.Screens;
import com.teremok.blankgame.util.BlankResourceManager;
import com.teremok.framework.TeremokGame;
import com.teremok.framework.util.Animation;
import com.teremok.framework.util.Localizator;

public class BlankGame extends TeremokGame {

	@Override
	public void create () {
        Gdx.app.setLogLevel(Application.LOG_ERROR);

        resourceManager = new BlankResourceManager(this);
        screenController = new BlankScreenController(this);

        resourceManager.preload();
        resourceManager.finishLoading();

        Localizator.init(this);

        Animation.DURATION_NORMAL = 0.0f;
        Animation.DURATION_SHORT = 0.0f;
        Animation.DURATION_LONG = 0.0f;

        screenController.setScreen(Screens.MAIN);
	}
}
